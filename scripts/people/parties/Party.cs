using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace People
{
    public class Party : Control
    {
        public string customName;

        public bool playerControlled = false;

        private Character _leader;
        public Character Leader
        {
            get
            {
                if (_leader == null)
                {
                    //TODO replace this with code to calculate who in a group is the leader based on personality
                    _leader = Members.First();
                }
                return _leader;
            }
            set
            {
                if (Members.Contains(value))
                {
                    _leader = value;
                }
                else
                {
                    GD.PrintErr($"Tried to set character: {value.Name} as leader of party: {this.Name} when character is not a member of party.");
                }
            }
        }

        private List<Character> _members = new List<Character>();
        public List<Character> Members
        {
            get
            {
                _members = this.GetChildren().OfType<Character>().ToList();
                return _members;
            }
        }
        
        public override void _Ready()
        {

        }
    }
}