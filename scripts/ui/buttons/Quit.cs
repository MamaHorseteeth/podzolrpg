using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace UI.Buttons
{
    public class Quit : ButtonAssembly
    {
        public override void _Ready()
        {
            base._Ready();

            isNavigator = false;
        }

        public override void ProcessButtonPress()
        {
            GlobalMain.QuitToDesktop();
        }
    }
}