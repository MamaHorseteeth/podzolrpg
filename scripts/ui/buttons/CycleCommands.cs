using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace UI.Buttons
{
    public class CycleCommands : ButtonAssembly
    {
        [Export]
        public string hotkeyText;

        //shortcuts
        private RichTextLabel _hotkeyLabel;
        public RichTextLabel HotkeyLabel
        {
            get
            {
                if (_hotkeyLabel == null)
                {
                    _hotkeyLabel = (RichTextLabel)GetNode("HotkeyLabel");
                }
                return _hotkeyLabel;
            }
        }

        public override void _Ready()
        {
            base._Ready();
            if (hotkeyText != null)
            {
                HotkeyLabel.BbcodeText = "[b]" + hotkeyText.Capitalize() + "[/b]";
            }
            DisableKey();
        }

        public void DisableKey()
        {
            this.Button.Disabled = true;
        }
    }
}