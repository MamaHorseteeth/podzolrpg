using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;
using UI.Widgets;

namespace UI.Buttons
{
    public abstract class ButtonAssembly : Button
    {
        //---Components---
        private Button _button;
        public Button Button
        {
            get
            {
                if (_button == null)
                {
                    _button = (Button)this;
                }
                return _button;
            }
        }

        private RichTextLabel _label;
        public RichTextLabel Label
        {
            get
            {
                if (_label == null)
                {
                    _label = (RichTextLabel)GetNode("Label");
                }
                return _label;
            }
        }

        //rather than have tooltips exist at all times in a hidden state until the button is hovered over, tooltips are generated on hovering and freed on 
        public bool displayTooltip = false;
        public string tooltipName;
        public string tooltipDescription;
        private readonly string tooltipPath = "res://scenes/ui/widgets/_TooltipBase.tscn";
        public Tooltip Tooltip { get; set; }



        //---Globals---
        private Globals.Main _globalMain;
        public Globals.Main GlobalMain
        {
            get
            {
                if (_globalMain == null)
                {
                    _globalMain = (Globals.Main)GetTree().Root.GetNode("Main");
                }
                return _globalMain;
            }
        }
        private Globals.Mapping _globalMapping;
        public Globals.Mapping GlobalMapping
        {
            get
            {
                if (_globalMapping == null)
                {
                    _globalMapping = (Globals.Mapping)GetTree().Root.GetNode("Mapping");
                }
                return _globalMapping;
            }
        }

        private Events.EventEngine _eventEngine;
        public Events.EventEngine EventEngine
        {
            get
            {
                if (_eventEngine == null)
                {
                    _eventEngine = (Events.EventEngine)GetTree().Root.GetNode("Game/EventEngine");
                }
                return _eventEngine;
            }
        }

        //---Logic---
        public bool isNavigator; //if the button navigates you to a new menu when you use it. does not include the back button
        public string buttonPath;//if navigable, where does the button lead
        public string navigationEventDefName; //if an event should run on menu switch, what is the name of that event's def?

        public override void _Ready()
        {
            Button.Connect("pressed", this, nameof(ProcessButtonPress));
            Button.Connect("mouse_entered", this, nameof(CreateTooltip));
            Button.Connect("mouse_exited", this, nameof(RemoveTooltip));
        }

        //every button needs to do something different, but connecting buttons to a signal has to be done at the ready. i hate working with signals, personally, so this seems the easiest way to do this
        public virtual void ProcessButtonPress()
        {
            if (isNavigator == true && !(buttonPath.Empty() || buttonPath == null))
            {
                //access the global list of menu filepaths previously accessed, and add the current menu to it
                //main knows what the current menu is, and a menu knows what its own filepath is
                GlobalMain.previousMenuPaths.Add(GlobalMain.CurrentMenu.menuReturnPath);

                //switch the menu to the target of the button path, additionally launching an event if set
                if (navigationEventDefName == null)
                {
                    GlobalMain.SwitchMenu(buttonPath);
                }
                else
                {
                    GlobalMain.SwitchMenu(buttonPath, true, navigationEventDefName);
                }
            }
            //get rid of the tooltip if the button's been pressed, as it's no longer relevant
            if (Tooltip != null)
            {
                Tooltip.QueueFree();
                Tooltip = null;
            }
        }

        public virtual void CreateTooltip()
        {
            if (displayTooltip == true && Button.Disabled == false)
            {
                Tooltip = (Tooltip)GlobalMain.LoadSceneFromFile(tooltipPath);
                AddChild(Tooltip);
                Tooltip.Display(tooltipName, tooltipDescription);
            }
        }

        public virtual void RemoveTooltip()
        {
            if (Tooltip != null)
            {
                Tooltip.QueueFree();
                Tooltip = null;
            }
        }
    }
}