﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace UI.Buttons
{
    public class NewGame : ButtonAssembly
    {
        public override void _Ready()
        {
            base._Ready();

            isNavigator = true;
            buttonPath = "res://scenes/ui/menus/PlayMenu.tscn";
            navigationEventDefName = "Core_NewGame";
        }

        public override void ProcessButtonPress()
        {
            base.ProcessButtonPress();
            StartNewGame();
        }

        public void StartNewGame()
        {
        }
    }
}