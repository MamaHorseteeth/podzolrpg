using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace UI.Buttons
{
    public class CommandButton : ButtonAssembly
    {
        [Export]
        public string hotkeyText;

        public Events.Event eventToRun;

        //shortcuts
        private RichTextLabel _hotkeyLabel;
        public RichTextLabel HotkeyLabel
        {
            get
            {
                if (_hotkeyLabel == null)
                {
                    _hotkeyLabel = (RichTextLabel)GetNode("HotkeyLabel");
                }
                return _hotkeyLabel;
            }
        }

        public override void _Ready()
        {
            base._Ready();

            //keyboard/keybinding compatibility can come later. for now, we're going to assume the 18 keys i like are the ones everyone will use
            //
            //sets the label to reflect the assigned key

            if (hotkeyText != null)
            {
                HotkeyLabel.BbcodeText = "[b]" + hotkeyText.Capitalize() + "[/b]";
            }
            ClearIfEmpty();
        }

        public override void ProcessButtonPress()
        {
            CallDeferred(nameof(ProcessCommand));
        }

        public void ProcessCommand()
        {
            base.ProcessButtonPress();

            //add the button's label text to the log, as well as a spacer to visually delineate events
            Menus.LogSubmenu logSubmenu = (Menus.LogSubmenu)GetTree().Root.GetNode("Game/PlayMenu/Control/SubmenuScreen/LogSubmenu");

            PackedScene packedSpacer = (PackedScene)GD.Load("res://scenes/ui/widgets/outputs/TextOutput.tscn");
            Widgets.TextOutput spacer = (Widgets.TextOutput)packedSpacer.Instance();
            spacer.Label.BbcodeText = $"[center]* * * * * * *[/center]";
            logSubmenu.LogEntries.AddChild(spacer);

            PackedScene packedCommand = (PackedScene)GD.Load("res://scenes/ui/widgets/outputs/TextOutput.tscn");
            Widgets.TextOutput previousCommand = (Widgets.TextOutput)packedCommand.Instance();
            previousCommand.Label.BbcodeText = $"==> [b]{Label.Text}[/b]";
            logSubmenu.LogEntries.AddChild(previousCommand);

            //run the event if it exists
            if (eventToRun != null)
            {
                EventEngine.RunEvent(eventToRun);
            }
        }

        public void Disable()
        {
            this.Button.Disabled = true;
        }

        public void Clear()
        {
            Disable();
            this.Label.Hide();
            this.HotkeyLabel.Hide();
        }

        public void ClearIfEmpty()
        {
            if (eventToRun == null && Label.Text.Empty() == true)
            {
                Clear();
            }
        }

        public void DefineFromCommand(Events.Command command)
        {
            this.Button.Disabled = false;

            Label.Show();
            HotkeyLabel.Show();
            Label.BbcodeText = $"[center]{command.buttonLabelText}[/center]";

            displayTooltip = command.displayTooltip;
            tooltipName = command.buttonLabelText; //for commands, just restate the name of the command in the header
            tooltipDescription = command.tooltipDescription;

            eventToRun = command.eventToRun;
        }
    }
}