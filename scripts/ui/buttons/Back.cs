﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace UI.Buttons
{
    public class Back : ButtonAssembly
    {
        public string backReturnPath;

        public override void _Ready()
        {
            base._Ready();

            isNavigator = false;
            //backReturnPath = GlobalMain.previousMenuPaths?.Last();
        }

        public override void ProcessButtonPress()
        {
            //because the back button is not classified as a navigator, the base button code for scene switching will not suit us
            //ReturnToPreviousMenu(backReturnPath);
        }

        public void ReturnToPreviousMenu(string path)
        {
            //calls the scene switcher, removes the last file path from the list of previous scenes accessed, and then, due to deference, actually runs the menu switcher
            GlobalMain.SwitchMenu(path);
            GlobalMain.previousMenuPaths.RemoveAt(GlobalMain.previousMenuPaths.Count() - 1);
        }
    }
}