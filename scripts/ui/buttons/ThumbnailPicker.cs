using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace UI.Buttons
{
    public class ThumbnailPicker : ButtonAssembly
    {

        public override void _Ready()
        {
            base._Ready();

            //probably should be true, but is false until it's functional
            isNavigator = false;
        }

        public override void ProcessButtonPress()
        {
            //this is where it would navigate to some kind of image uploading apparatus
        }
    }
}