using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace UI.Menus
{
    public class Menu : CanvasLayer
    {
        public string menuReturnPath
        {
            get => Filename;
        }

        //---Globals---
        private Globals.Main _globalMain;
        private Globals.Main GlobalMain
        {
            get
            {
                if (_globalMain == null)
                {
                    _globalMain = (Globals.Main)GetTree().Root.GetNode("Main");
                }
                return _globalMain;
            }
        }

        public override void _Ready()
        {

            //adds the current menu to the list of menus previously accessed
            GlobalMain.previousMenuPaths.Add(menuReturnPath);
        }
    }
}