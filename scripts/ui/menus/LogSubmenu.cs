using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Defs;
using Godot;
using UI.Widgets;

namespace UI.Menus
{
    public class LogSubmenu : Control
    {
        //---Logic--

        double maxScroll;
        
        //---Components---
        
        private Control _log;
        public Control Log
        {
            get
            {
                if (_log == null)
                {
                    _log = (Control)GetNode("Log");
                }
                return _log;
            }
        }

        private ScrollContainer _scrollContainer;
        public ScrollContainer ScrollContainer
        {
            get
            {
                if (_scrollContainer == null)
                {
                    _scrollContainer = (ScrollContainer)Log.GetNode("ScrollContainer");
                }
                return _scrollContainer;
            }
        }

        private VScrollBar _scrollBar;
        public VScrollBar ScrollBar
        {
            get
            {
                if (_scrollBar == null)
                {
                    _scrollBar = ScrollContainer.GetVScrollbar();
                }
                return _scrollBar;
            }
        }


        private VBoxContainer _logEntries;
        public VBoxContainer LogEntries
        {
            get
            {
                if (_logEntries == null)
                {
                    _logEntries = (VBoxContainer)ScrollContainer.GetNode("LogEntries");
                }
                return _logEntries;
            }
        }

        //---Globals---

        private Globals.Main _globalMain;
        public Globals.Main GlobalMain
        {
            get
            {
                if (_globalMain == null)
                {
                    _globalMain = (Globals.Main)GetTree().Root.GetNode("Main");
                }
                return _globalMain;
            }
        }

        public override void _Ready()
        {
            ScrollBar.Connect("changed", this, nameof(ScrollToBottom));
            maxScroll = ScrollBar.MaxValue;
        }

        public void ScrollToBottom()
        {
            ScrollBar.Value = ScrollBar.MaxValue;

            if (maxScroll != ScrollBar.MaxValue)
            {

                maxScroll = ScrollBar.MaxValue;
                ScrollBar.Value = ScrollBar.MaxValue;
            }
        }
    }
}

