using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace UI.Widgets
{
    public class LineInput : LogOutput
    {
        private RichTextLabel _pretext;
        public RichTextLabel Pretext
        {
            get
            {
                if (_pretext == null)
                {
                    _pretext = (RichTextLabel)GetNode("HBoxContainer/Pretext");
                }
                return _pretext;
            }
        }

        private LineEdit _textField;
        public LineEdit TextField
        {
            get
            {
                if (_textField == null)
                {
                    _textField = (LineEdit)GetNode("HBoxContainer/TextField");
                }
                return _textField;
            }
        }

        public override void _Ready()
        {

        }
    }
}