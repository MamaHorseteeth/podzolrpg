using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace UI.Widgets
{
    public class TextOutput : LogOutput
    {
        private RichTextLabel _label;
        public RichTextLabel Label
        {
            get
            {
                if(_label == null)
                {
                    _label = (RichTextLabel)GetNode("Text");
                }
                return _label;
            }
        }


        public override void _Ready()
        {
            
        }
    }
}