using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;
using People;

namespace UI.Widgets
{
    public class CharacterViewer : Control
    {
        //---Logic---
        
        private Character _currentCharacter;
        public Character CurrentCharacter
        {
            get 
            {
                return _currentCharacter;
            }
            set
            {
                _currentCharacter = value;
                UpdateDisplay();
            }
        }

        //---Components---
        private RichTextLabel _characterNameLabel;
        public RichTextLabel CharacterNameLabel
        {
            get
            {
                if (_characterNameLabel == null)
                {
                    _characterNameLabel = (RichTextLabel)GetNode("Header/HeaderTexts/Name/Label");
                }
                return _characterNameLabel;
            }
        }

        public override void _Ready()
        {

        }

        public void UpdateDisplay()
        {
            //update the name to reflect the character's name
            CharacterNameLabel.BbcodeText = $"[center]{CurrentCharacter.Name}[/center]";
        }
    }
}