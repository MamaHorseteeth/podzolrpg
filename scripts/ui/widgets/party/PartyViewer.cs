using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;
using People;

namespace UI.Widgets
{
    public class PartyViewer : Control
    {
        //---Logic---

        private Party _currentParty;
        public Party CurrentParty
        {
            get
            {
                return _currentParty;
            }
            set
            {
                _currentParty = value;
                UpdateDisplay();
            }
        }

        //---Components---

        private VBoxContainer _characterListContainer;
        public VBoxContainer CharacterListContainer
        {
            get
            {
                if (_characterListContainer == null)
                {
                    _characterListContainer = (VBoxContainer)GetNode("ScrollContainer/CharacterList");
                }
                return _characterListContainer;
            }
        }

        private RichTextLabel _headerLabel;
        public RichTextLabel HeaderLabel
        {
            get
            {
                if (_headerLabel == null)
                {
                    _headerLabel = (RichTextLabel)GetNode("Header/RichTextLabel");
                }
                return _headerLabel;
            }
        }

        //---Globals---

        private Globals.Main _globalMain;
        public Globals.Main GlobalMain
        {
            get
            {
                if (_globalMain == null)
                {
                    _globalMain = (Globals.Main)GetTree().Root.GetNode("Main");
                }
                return _globalMain;
            }
        }

        public override void _Ready()
        {

        }
        
        private void UpdateDisplay()
        {
            //part of me wonders whether it would be significant or even more performant to simply clear the characterviewers on update every time and repopulate them, than to perform checks whether we need to get rid of or add each character viewer
            //idk if i'm going to actually let parties get biger than a handful of characters anyways, which is where i think the iteration through the list would actually matter so much
            
            //go through the characterviewers and toss out viewers for anyone who isn't in the current party
            IEnumerable<CharacterViewer> characterViewers = CharacterListContainer.GetChildren().OfType<CharacterViewer>();
            foreach(CharacterViewer characterViewer in characterViewers)
            {
                if (!CurrentParty.Members.Contains(characterViewer.CurrentCharacter))
                {
                    characterViewer.QueueFree();
                }
            }

            //add any characterviewers for characters who are in the current party but do not have a characterviewer
            for (int x = 0; x < CurrentParty.Members.Count; x++)
            {
                bool partyHasCharacter = false;
                foreach(CharacterViewer characterViewer in characterViewers)
                {
                    if (characterViewer.CurrentCharacter == CurrentParty.Members[x])
                    {
                        partyHasCharacter = true;
                        break;
                    }
                }
                if (partyHasCharacter == false)
                {
                    CharacterViewer newCharacterViewer = (CharacterViewer)GlobalMain.LoadSceneFromFile("res://scenes/ui/widgets/party/CharacterViewer.tscn");
                    //this setter property runs CharacterViewer.Update() so that's all we need to do to the new viewer at this level
                    newCharacterViewer.CurrentCharacter = CurrentParty.Members[x];
                    CharacterListContainer.AddChild(newCharacterViewer);
                }
            }

            //if the viewer at the container's child index 0 is not the leader's viewer, get the leader's viewer and move it to the 0th index
            if (CharacterListContainer.GetChild<CharacterViewer>(0).CurrentCharacter != CurrentParty.Leader)
            {
                CharacterViewer leaderViewer = characterViewers.Where(viewer => viewer.CurrentCharacter == CurrentParty.Leader).First();
                CharacterListContainer.MoveChild(leaderViewer, 0);
            }

            //relabel the party viewer as either the party name, the name of the only character if alone, or "[leadername]'s party" if not alone
            if (CurrentParty.customName != null && !CurrentParty.customName.Empty())
            {
                HeaderLabel.BbcodeText = $"[center][b]{CurrentParty.customName}[/b][/center]";
            }
            else if (CurrentParty.Members.Count == 1)
            {
                HeaderLabel.BbcodeText = $"[center][b]{CurrentParty.Leader.Name}[/b][/center]";
            }
            else if (CurrentParty.Members.Count > 1)
            {
                HeaderLabel.BbcodeText = $"[center][b]{CurrentParty.Leader.Name}'s Party[/b][/center]";
            }
        }
    }
}

