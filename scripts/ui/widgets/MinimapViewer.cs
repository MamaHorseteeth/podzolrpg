using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace UI.Widgets
{
    public class MinimapViewer : PanelContainer
    {
        private AspectRatioContainer _aspectRatioContainer;
        public AspectRatioContainer AspectRatioContainer
        {
            get
            {
                if(_aspectRatioContainer == null)
                {
                    _aspectRatioContainer = (AspectRatioContainer)GetNode("AspectRatioContainer");
                }
                return _aspectRatioContainer;
            }
        }
        
        public override void _Ready()
        {
            //aspect ratio containers are janky, so we need to dynamically update the height of the minimap every time the width changes sides
            RectMinSize = new Vector2(AspectRatioContainer.RectSize.x, AspectRatioContainer.RectSize.x);
            //Connect("resized", this, nameof(ResizeMinimap));
        }

        public void ResizeMinimap()
        {
            //the panel needs to get the size of the aspect container within
            //RectSize = new Vector2(AspectRatioContainer.RectSize.x, AspectRatioContainer.RectSize.x);
        }
    }
}

