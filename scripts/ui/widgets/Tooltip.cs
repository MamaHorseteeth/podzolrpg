using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace UI.Widgets
{

    public class Tooltip : Popup
    {
        //---Components---
        private VBoxContainer _labelContainer;
        public VBoxContainer LabelContainer
        {
            get
            {
                if (_labelContainer == null)
                {
                    _labelContainer = (VBoxContainer)GetNode("Panel/LabelContainer");
                }
                return _labelContainer;
            }
        }
        private RichTextLabel _nameLabel;
        public RichTextLabel NameLabel
        {
            get
            {
                if (_nameLabel == null)
                {
                    _nameLabel = (RichTextLabel)LabelContainer.GetNode("NameLabel");
                }
                return _nameLabel;
            }
        }
        private RichTextLabel _descriptionLabel;
        public RichTextLabel DescriptionLabel
        {
            get
            {
                if (_descriptionLabel == null)
                {
                    _descriptionLabel = (RichTextLabel)LabelContainer.GetNode("DescriptionLabel");
                }
                return _descriptionLabel;
            }
        }


        public override void _Ready()
        {

        }

        public virtual void Display(string tooltipName, string tooltipDescription)
        {
            Vector2 mousePosition = GetViewport().GetMousePosition();

            //set the labels, because they determine the tooltip's size
            if (tooltipName != null)
            {
                NameLabel.BbcodeText = $"[center][b]{tooltipName}[/b][/center]";
            }
            if (tooltipDescription != null)
            {
                DescriptionLabel.BbcodeText = tooltipDescription;
            }

            //set the tooltip height to the vboxcontainer's height + the vboxcontainer's top and bottom margins
            int scaledYSize = (int)LabelContainer.RectSize.y + ((int)LabelContainer.MarginTop * 2); //+ (int)LabelContainer.MarginBottom;
            this.RectSize = new Vector2(RectSize.x, scaledYSize);

            //position the tooltip at the mouse's current position, with provisions for if the tooltip would display offscreen
            //we can consider thinning the tooltip to a lower min size before repositioning later, as polish
            Vector2 screenSize = GetViewport().Size;

            int rightmostX = (int)mousePosition.x + (int)this.RectSize.x;
            int repositionedX = (int)mousePosition.x;
            if (rightmostX > screenSize.x)
            {
                int excessX = rightmostX - (int)screenSize.x;
                repositionedX -= excessX;
            }

            int repositionedY = (int)mousePosition.y;
            int lowermostY = (int)mousePosition.y + (int)this.RectSize.y;
            if (lowermostY > screenSize.y)
            {
                int excessY = lowermostY - (int)screenSize.y;
                repositionedY -= excessY;
            }
            this.RectPosition = new Vector2(repositionedX, repositionedY);

            this.Show();
        }
    }
}