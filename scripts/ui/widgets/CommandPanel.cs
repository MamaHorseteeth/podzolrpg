using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Events;
using Godot;
using UI.Buttons;

namespace UI.Widgets
{
    public class CommandPanel : Panel
    {
        public int buttonsPerCycle = 18; //i'm not planning on making the grid any larger than it is, but if i do, at least the count is centrally-located
        public int commandCount; //number of commands currently registered by the command panel
        private int _highestReservedIndex = -1;
        public int HighestReservedIndex //highest index reserved by a command
        {
            get
            {
                //if not yet initialized and command queue is populated, then we need to initialize it. set it to the highest-value int in the keys of the queue
                if (reservationsInitialized == false && commandQueue.Any() == true)
                {
                    _highestReservedIndex = commandQueue.Keys.Max();
                    reservationsInitialized = true;
                }
                //if not initialized and queue also null, remain null
                //if initialized and queue has nothing, something has set the value for its own reasons, which may or may not be right. leave it alone
                //if initialized and the queue is populated, then the value is already initialized and may have since been modified. leave it alone
                return _highestReservedIndex;
            }
            set { _highestReservedIndex = value; }
        }
        private bool reservationsInitialized = false;

        public int currentCycle;
        public int ButtonCycleCount //number of "screens" of buttons accessible; the cycle buttons at either side of the command keys move between the cycles
        {
            //the number of cycles we'll need is the highest index (+1 for count correlation) divided by the number of buttons per cycle, then rounded up to the nearest whole number
            get { return (int)Math.Ceiling(((float)HighestReservedIndex + 1) / (float)buttonsPerCycle); }
        }

        //no longer necessary as of the removal of all containers from this scene
        //public string gridPath = "MarginContainer/HBoxContainer/MarginContainer/CommandKeyGrid"; //the current highest-level container above the buttons

        
        public int TotalSlots { get { return buttonsPerCycle * ButtonCycleCount; } }

        public Dictionary<int, Command> commandQueue = new Dictionary<int, Command>();

        //---Globals---

        public override void _Ready()
        {

        }

        //takes the commands from an event def and turns them into button displays
        public void AssignEventCommandsToButtons(List<Command> commands)
        {
            //first, clear the existing buttons, as they're no longer supposed to be accessible
            ClearOldButtons();
            //now that the buttons themselves are irrelevant, also empty the dictionary of indices, as we need it empty before we repopulate it
            commandQueue.Clear();

            //next, we have to index our commands, to learn where they go and how many cycles we need

            //we start with the commands that reserve an index, to ensure the slots they ask for are available
            IndexReserveIndexCommands(commands);
            //next in line are the commands that take the first free slot
            IndexFirstAvailableCommands(commands);
            //finally, the commands that take the last possible slots, which need to know how many slots there are to take
            IndexLastAvailableCommands(commands);

            //always reset the current cycle to the first one; that is to say, always display the first screen of possible commands whenever an event is fired
            currentCycle = 0;
            //then populate the buttons at their indices
            PopulateButtons();

        }

        public void IndexReserveIndexCommands(List<Command> commands)
        {
            IEnumerable<Command> reservingCommands = commands?.Where(command => command.buttonQueueLogic == "ReserveIndex");
            

            if (reservingCommands != null)
            {
                foreach (Command command in reservingCommands)
                {
                    //add to command panel's dictionary of indices-commands at the listed index, throwing an error if index is unlisted or already taken
                    try
                    {
                        commandQueue.Add(command.reservedIndex, command);
                    }
                    catch (Exception)
                    {
                        GD.PrintErr($"Failed to add command named: \"{command.buttonLabelText}\" with queue logic: ReserveIndex and reserved index: {command.reservedIndex} to command panel. Is index null or duplicate?");
                    }
                }
            }
        }

        public void IndexFirstAvailableCommands(List<Command> commands)
        {
            //we assume a queue with any pre-reserved commands to potentially have gaps until we can confirm it has no gaps during the assignment of FirstAvailable commands
            bool queueHasGaps = false;
            if (HighestReservedIndex < -1) { queueHasGaps = true; }

            IEnumerable<Command> firstAvailableCommands = commands?.Where(command => command.buttonQueueLogic == "FirstAvailable");
            if (firstAvailableCommands != null)
            {
                foreach (Command command in firstAvailableCommands)
                {
                    bool commandAddedInGap = false;
                    //if the queue has or may have gaps where indices are not taken below the highest index, find the lowest index not already taken and add the command there
                    if (queueHasGaps == true)
                    {
                        for (int y = 0; y <= HighestReservedIndex; y++)
                        {
                            if (!commandQueue.ContainsKey(y))
                            {
                                commandQueue.Add(y, command);
                                commandAddedInGap = true;
                                break;
                            }
                        }
                    }
                    //if we haven't yet added the command and all slots lower than the highest reserved index are taken, increment the highest reserved index by 1, affirm the queue has no gaps, and add the command at the new highest reserved index
                    if (commandAddedInGap == false)
                    {
                        queueHasGaps = false;
                        HighestReservedIndex += 1;
                        commandQueue.Add(HighestReservedIndex, command);
                    }
                }
            }
        }

        public void IndexLastAvailableCommands(List<Command> commands)
        {
            IEnumerable<Command> lastAvailableCommands = commands?.Where(command => command.buttonQueueLogic == "LastAvailable");
            if (lastAvailableCommands != null)
            {
                //first verify that there is room for however many last available commands there are at the end of the cycle
                for (int x = TotalSlots; x > (TotalSlots - lastAvailableCommands.Count()); x--)
                {
                    //if any slot at any index key is already reserved, add (number of slots per cycle) to the highest reserved value, which may even still not be enough if we have a metric buttload of LastAvailable commands
                    if (commandQueue.ContainsKey(x))
                    {
                        HighestReservedIndex += buttonsPerCycle;
                        //reset the loop. if this works properly, TotalSlots the property gets ButtonCycleCount the property, which in turn calculates the # of cycles using the new highest reserved index
                        x = TotalSlots;
                    }
                    //if the command queue doesn't contain the index key, then do nothing for now
                }

                //set the highest reserved index to the last possible index. idk if i actually need this anymore, but it's worth keeping accurate
                HighestReservedIndex = TotalSlots - 1; //-1 because total slots is a count so the final index is count - 1

                //now that we have ensured that there are enough empty slots at the end of the queue, start assigning the slots in reverse from the very end
                int increment = 0;
                foreach (Command command in lastAvailableCommands)
                {
                    commandQueue.Add(HighestReservedIndex - increment, command);
                    increment++;
                }
            }
        }

        public void ClearOldButtons()
        {
            for (int x = 0; x < buttonsPerCycle; x++)
            {
                CommandButton button = (CommandButton)this.GetNode(x.ToString());
                button.Clear();
            }
        }

        //uses the current cycle to populate the actual buttons with commanddef data
        public void PopulateButtons()
        {
            //get the indices of all commands in the commandQueue where the index is between curCyc*buttonsPerCycle and ((curCyc+1)butpCyc) - 1
            IEnumerable<int> currentCommandIndices = commandQueue.Keys?.Where(key => (key >= currentCycle * buttonsPerCycle) && (key < (currentCycle + 1) * buttonsPerCycle));

            //for each key...
            if (currentCommandIndices != null)
            {
                foreach (int index in currentCommandIndices)
                {
                    //get the command def at the index
                    Command command = commandQueue[index];

                    //get the command button with the modulated index
                    CommandButton button;
                    int modIndex = index % buttonsPerCycle;
                    try
                    {
                        button = (CommandButton)this.GetNode(modIndex.ToString());
                        button.DefineFromCommand(command);
                    }
                    catch (Exception e)
                    {
                        GD.PrintErr($"Failed to get command button with index {modIndex} . Check if path from widget to buttons was changed. Error: {e}");
                    }
                }
            }
        }
    }
}