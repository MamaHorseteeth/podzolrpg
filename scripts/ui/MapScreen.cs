using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;
using Mapping;

namespace UI
{
    public class MapScreen : CanvasLayer
    {
        public List<Map> cachedMaps = new List<Map> { }; //cache of maps that have been created or loaded this session
        public Dictionary<string, int> generatedMaps = new Dictionary<string, int> { }; //dict of maps generated this session (sorted by their resource path); and how many

        //---Shortcuts---
        Globals.Main globalMain;
        Globals.Mapping globalMapping;

        public Panel panel;
        public Map map;
        
        public override void _Ready()
        {
            globalMain = GetNode<Globals.Main>("/root/Main");
            globalMapping = GetNode<Globals.Mapping>("/root/Mapping");

            
        }

        public void ChangeMap(string mapResourcePath, bool cachePreviousMap)
        {
            Map backup = map;

            //first we have to get rid of the current map, if there is one
            if (map != null)
            {
                ////save the map to the save file's maps folder if instructed
                //if (savePreviousMap == true)
                //{
                //    //code to save the previous map to a file
                //    //on second thought, maybe this should be handled in the general saving code, because it corrupt your save if you'd leveled up or picked up items since your last save, but every map you've used had been saved already
                //}

                //hide the map and set it and its children to non-interactible, which is functionally analogous to having it cached
                //also adds it to the list of cached maps
                //maps that are cached in this way will not be reloaded on closing and reopening the program
                if (cachePreviousMap == true)
                {
                    //if the map is not already in the cache
                    if (!cachedMaps.Contains(map))
                    {
                        cachedMaps.Add(map);
                        map.Hide();
                        map.MouseFilter = Control.MouseFilterEnum.Ignore;
                    }
                }
                //if we're not caching it, then delete it, as we don't need the resource drain
                else
                {
                    map.QueueFree();
                }
            }

            //check the cache for an existing map with the same path
            //resource paths to make a new map should never be saved to that map's save path, so those should run through this loop without hitting anything
            //actual save paths, meanwhile, may or may not hit something
            //if a match is found, unhide it, make it stop ignoring mouse filter, and stop the method there
            for (int x = 0; x < cachedMaps.Count; x++)
            {
                if (cachedMaps[x].savePath == mapResourcePath)
                {
                    Map match = cachedMaps[x];
                    match.Show();
                    match.MouseFilter = Control.MouseFilterEnum.Stop;
                    return;
                }
            }

            //so if we've made it this far, we know that we have to load the map now
            try
            {
                //preload and instantiate the map from the provided file path
                PackedScene nextMap = (PackedScene)GD.Load(mapResourcePath);
                map = (Map)nextMap.Instance();
            }
            catch (Exception e)
            {
                GD.PrintErr("Error: tried to change map with an invalid resource path: " + mapResourcePath + " . Restoring backup. Error code: " + e);
                //tries the function over with the previous map, and does not cache the cunt
                ChangeMap(backup.savePath, false);
                return;
            }

            //if the map has no save name, then that indicates that it's freshly generated and we need to do some things
            if (map.savePath == null)
            {
                //+1 to the # of generations this map has done
                if (!generatedMaps.ContainsKey(mapResourcePath))
                {
                    generatedMaps.Add(mapResourcePath, 1);
                }
                else
                {
                    generatedMaps[mapResourcePath] += 1;
                }
                map.serialNumber = generatedMaps[mapResourcePath];

                //generate a save path for the map to use at saves/[save file name]/maps/[map node name][serial number]
                //(note: we have to make sure every save file generates with a maps subfolder)
                string newSavePath = "res://saves/" + globalMain.saveFileName + "/maps/" + map.Name + map.serialNumber;
                map.savePath = newSavePath;
            }

            //add the map to the map screen
            globalMapping.MapScreen.AddChild(map);
        }
    }
}

