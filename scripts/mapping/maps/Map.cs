using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace Mapping
{
    //What is a map?
    //a map (not the map screen) is a grid containing tiles and spaces, usually randomly generated
    //maps are added as children to the map screen when created and loaded, and are saved when exited as scenes that can be loaded back up the same way generic maps can
    //map node names should ALWAYS be the same as their resource names
    public class Map : Control
    {
        //---Generation Variables---
        public string savePath;
        public int serialNumber;
        private bool wasGenerated = false;

        [Export] //use integers please
        public Vector2 dimensions;

        //biome is what selection of tiles to draw from for a map
        //i'm not actually convinced that a dictionary is the best way to handle this. we might want to look into databases and parsing
        public Dictionary<string, string> biomeTiles = new Dictionary<string, string>{
            {"basic" , "res://scripts/mapping/tiles/Tile.cs"},
        };

        //---Shortcuts---
        private Globals.Main _globalMain;
        public Globals.Main GlobalMain
        {
            get
            {
                if (_globalMain == null)
                {
                    _globalMain = (Globals.Main)GetTree().Root.GetNode("Main");
                }
                return _globalMain;
            }
        }
        private Globals.Mapping _globalMapping;
        public Globals.Mapping GlobalMapping
        {
            get
            {
                if (_globalMapping == null)
                {
                    _globalMapping = (Globals.Mapping)GetTree().Root.GetNode("Mapping");
                }
                return _globalMapping;
            }
        }

        private List<Tile> _tiles = new List<Tile>();
        public List<Tile> Tiles
        {
            get
            {
                _tiles = (List<Tile>)this.GetChildren().OfType<Tile>();
                return _tiles;
            }
        }
        public List<Exit> Exits;

        private List<People.Party> _parties = new List<People.Party>();
        public List<People.Party> Parties
        {
            get
            {
                _parties = (List<People.Party>)this.GetChildren().OfType<People.Party>();
                return _parties;
            }
        }

        public override void _Ready()
        {
            //if the map has not been generated, generate it. if it already has, such as by loading a saved map, don't generate it
            //if (wasGenerated == false)
            //{
            //    GenerateMap();
            //    wasGenerated = true;

            //    SetMapName();
            //}
        }

        //---GENERATION MAIN INSTRUCTIONS---

        //pretty much every derived map should have its own protocols for generating a structure
        public virtual void GenerateMap()
        {
            GenerateTiles();

            //for each child of the map that is typed as a Tile, generate the exits for that tile
            for (int x = 0; x < Tiles.Count; x++)
            {
                GenerateExits(Tiles[x]);
            }

        }

        public virtual void GenerateTiles()
        {

            //using the dimensions provided, we generate a map full of default tiles. ignore that we're using tilebase right now
            //we fill the map in the same order that one reads words from a book
            for (int i = 0; i < dimensions.y; i++)
            {
                for (int j = 0; j < dimensions.x; j++)
                {
                    //gets a random tile from a chosen biome, loads, and instances it
                    string tileResourcePath = GetRandomTileResourcePath(biomeTiles);
                    PackedScene tilePacked = (PackedScene)GD.Load(tileResourcePath);
                    Tile tileInstance = tilePacked.Instance<Tile>();

                    //assigns the tile's position to the current i/j combo in the loop
                    tileInstance.mapPosition = new Vector2(i, j);

                    //finally the tile is added to the scene as a child of the map, and its own map shortcut is filled in
                    AddChild(tileInstance);
                    tileInstance.Map = tileInstance.GetParent<Map>();
                }
            }
        }

        public virtual void GenerateExits(Tile origin)
        {
            //our default behavior will be to generate an exit at every possible location on a tile
            //change that if you want your exits to generate differently
            GenerateExitsInEveryPossibleDirection(origin, 1);
        }

        //---GENERATION SUB-INSTRUCTIONS---

        public void GenerateExitsInEveryPossibleDirection(Tile origin, float exitMagnitude)
        {
            //replace this later to get a biome-appropriate random not-blocked, not-hidden exit

            //for each valid direction, make an exit
            for (int x = 0; x < GlobalMapping._validDirections.Count; x++)
            {
                PackedScene exitPacked = (PackedScene)GD.Load("res://scenes/mapping/exits/_Base.tscn");
                Exit exitInstance = exitPacked.Instance<Exit>();

                exitInstance.origin = origin;

                //now we determine the direction
                //shortcut for the key at the current index
                string indexedDirectionKey = GlobalMapping._validDirections.ElementAt(x).Key;

                //at some point we'd want to change the direction here to a line, i.e. both north and south as directions would become the line NS or something. but i'll do that when I need to

                exitInstance.Direction = indexedDirectionKey;

                //now we get the endpoint by looking for a tile in the given direction and magnitude from the origin
                Tile endpoint = origin.GetTileFromCurrentTile(exitInstance.Direction, exitMagnitude, exitMagnitude);
                //if we haven't found a valid endpoint (for instance because the origin, direction, and magnitude do not point to a tile, or even point to a location off the map) delete the exit and start the loop over in a new direction
                if (endpoint == null)
                {
                    exitInstance.QueueFree();
                    continue;
                }
                //otherwise, we can now actually assign local endpoint to the exit
                exitInstance.destination = endpoint;

                //now we check if there is any existing exit that is the same class of exit, has either the same origin & endpoint, or reversed origin/endpoint, because such exits would be redundant
                IEnumerable<Exit> duplicateExits = Exits.Where(
                    exit =>
                    exit.GetClass() == exitInstance.GetClass()
                    &&
                    ((exit.origin == exitInstance.origin && exit.destination == exitInstance.destination)
                    ||
                    (exit.origin == exitInstance.destination && exit.destination == exitInstance.origin))
                    );
                //if so, we delete the current exit and start the loop over in a new direction
                if (duplicateExits.Any())
                {
                    exitInstance.QueueFree();
                    continue;
                }

                //add the exit to the tile-side list of exits, and to the scene tree as a child of the origin
                exitInstance.AddToTile(origin);
            }
        }

        //---FETCHERS---
        public string GetRandomTileResourcePath(Dictionary<string, string> sampleDictionary)
        {
            //picks a tile at random from the possible tiles
            int r = (int)(GD.Randi() % sampleDictionary.Count);

            GD.Print();
            return sampleDictionary.ElementAt(r).Value;
        }

        //---MISC---

        public void SetMapName()
        {
            this.Name = "TestMap";
            //in the future we'll want to adjust the naming system so that it appends a serial number to the map, allowing us to keep multiple saved scenes of a map generated from a single template scene
        }
    }
}