using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace Mapping
{
    //what is a tile?
    //a tile is a division of the map that contains any number of interactible things like characters, items, and exits
    //all tiles exist on a map; a tile cannot exist on its own
    public class Tile : Control
    {

        private Map _map;
        public Map Map
        {
            get => _map;
            set => _map = value;
        }
        public Vector2 mapPosition;
        public bool IsOnEdgeOfMap
        {
            get
            {
                //the -1 represents the fact the largest possible position number is dimensions - 1
                //so if x or y are 0 or that largest possible number, the tile is on the edge of the map
                if (mapPosition.x == 0 || mapPosition.x == Map.dimensions.x - 1 || mapPosition.y == 0 || mapPosition.y == Map.dimensions.y)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        public List<Exit> exits = new List<Exit>();

        //---Globals---
        private Globals.Main _globalMain;
        public Globals.Main GlobalMain
        {
            get
            {
                if (_globalMain == null)
                {
                    _globalMain = (Globals.Main)GetTree().Root.GetNode("Main");
                }
                return _globalMain;
            }
        }
        private Globals.Mapping _globalMapping;
        public Globals.Mapping GlobalMapping
        {
            get
            {
                if (_globalMapping == null)
                {
                    _globalMapping = (Globals.Mapping)GetTree().Root.GetNode("Mapping");
                }
                return _globalMapping;
            }
        }



        public override void _Ready()
        {

        }

        //do note that we're not preserving the dimensions of non-grid space here. the tile to the direct southeast is considered 1 tile away, not ~1.4
        public Tile GetTileFromCurrentTile(string direction, float xMagnitude, float yMagnitude)
        {
            float xDistance = 0;
            bool changesX = (GlobalMapping._validDirections[direction][0] != null);
            if (changesX == true)
            {
                int xPolarity = 1;
                if (GlobalMapping._validDirections[direction][0] == false)
                {
                    xPolarity = -1;
                };
                xDistance = xPolarity * xMagnitude;
            }

            float yDistance = 0;
            bool changesY = (GlobalMapping._validDirections[direction][1] != null);
            if (changesY == true)
            {
                int yPolarity = 1;
                if (GlobalMapping._validDirections[direction][0] == false)
                {
                    yPolarity = -1;
                };
                yDistance = yPolarity * yMagnitude;
            }

            //gets any tiles at the designated x and y positions
            IEnumerable<Tile> targets = Map.Tiles.Where(
                tile => tile.mapPosition.x == this.mapPosition.x + xDistance
                &&
                tile.mapPosition.y == this.mapPosition.y + yDistance
                );
            //if we got anything, return the first target; else return null
            //we SHOULDN'T ever have multiple tiles occupying the same position, but if we do, all but the first will be ignored
            if (targets.Any())
            {
                Tile target = targets.First();
                return target;
            }
            else
            {
                return null;
            }
        }

        public bool IsOnEdgeOfMapInDirection(string direction)
        {
            string aliasedDirection = (string)GlobalMain.TryFindAlias(GlobalMapping.directionAliases, direction);
            if (aliasedDirection != null)
            {
                direction = aliasedDirection;
            }
            else
            {
                GD.PrintErr("Error: Failed to find alias: '" + direction + "' in alias Dictionary: " + nameof(GlobalMapping.directionAliases) + ". Check for typoes or add a new alias to the dictionary.");
            }

            //there's gotta be a simpler way to do this
            if (direction == "north" && mapPosition.y == 0)
            {
                return true;
            }
            else if (direction == "south" && mapPosition.y == Map.dimensions.y - 1)
            {
                return true;
            }
            else if (direction == "west" && mapPosition.x == 0)
            {
                return true;
            }
            else if (direction == "east" && mapPosition.x == Map.dimensions.x - 1)
            {
                return true;
            }
            //maps are rectangular by implementation so there should never be a situation in which a tile is on, say, the northern edge of a map yet has a neighbor to its northwest
            else if (direction == "northwest" && (mapPosition.y == 0 || mapPosition.x == 0))
            {
                return true;
            }
            else if (direction == "northeast" && (mapPosition.y == 0 || mapPosition.x == Map.dimensions.x - 1))
            {
                return true;
            }
            else if (direction == "southwest" && (mapPosition.y == 0 || mapPosition.x == 0))
            {
                return true;
            }
            else if (direction == "southeast" && (mapPosition.y == Map.dimensions.y - 1 || mapPosition.x == 0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}