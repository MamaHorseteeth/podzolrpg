using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace Mapping
{
    //what is an exit?
    //an exit is an interaction point on a tile that shows up on the map as an arrow between its origin and its endpoint
    //exit scenes are children of their origin tiles
    //an exit is one-sided: it is not guaranteed that an exit's destination contains a corresponding exit leading back to the origin
    public class Exit : Node2D
    {
        //---Globals---
        private Globals.Main _globalMain;
        public Globals.Main GlobalMain
        {
            get
            {
                if (_globalMain == null)
                {
                    _globalMain = (Globals.Main)GetTree().Root.GetNode("Main");
                }
                return _globalMain;
            }
        }
        private Globals.Mapping _globalMapping;
        public Globals.Mapping GlobalMapping
        {
            get
            {
                if (_globalMapping == null)
                {
                    _globalMapping = (Globals.Mapping)GetTree().Root.GetNode("Mapping");
                }
                return _globalMapping;
            }
        }
        
        //---Interactivity---
        public bool isHidden = false;
        public bool isBlocked = false;

        //---Locationing---
        public Tile origin; //the parent node, which should be a tile
        public Tile destination;

        private Vector2 _centerpoint;
        public Vector2 Centerpoint
        {
            get
            {
                if (origin != null && destination != null)
                {
                    //simple as averaging the x and y values of the tiles' positions
                    float centerX = (origin.mapPosition.x + destination.mapPosition.x) / 2;
                    float centerY = (origin.mapPosition.y + destination.mapPosition.y) / 2;
                    _centerpoint = new Vector2(centerX, centerY);
                }
                else
                {
                    GD.PrintErr("Error: tried to find centerpoint of an exit missing either an origin or an endpoint.");
                }
                return _centerpoint;
            }
        }

        private string _direction;
        public string Direction
        {
            get => _direction;
            set
            {
                //any attempt to set the direction with an accepted alias (i.e. "n" for "north") will automatically set the direction to the single standard alias for that direction
                string aliasedDirection = (string)GlobalMain.TryFindAlias(GlobalMapping.directionAliases, value);
                if (aliasedDirection != null)
                {
                    _direction = aliasedDirection;
                }
                else
                {
                    GD.PrintErr("Error: Failed to find alias: '" + value + "' in alias Dictionary: " + nameof(GlobalMapping.directionAliases) + ". Check for typoes or add a new alias to the dictionary.");
                }
            }
        }
        
        //for managing map transitions
        //maybe make map transitions a subclass instead?
        public bool isMapExit;
        public string newMapResourcePath;
        public string savedMapExitPath;

        

        

        public override void _Ready()
        {
            
        }

        public void AddToTile(Tile origin)
        {
            origin.AddChild(this);
            origin.exits.Add(this);
        }
    }
}
