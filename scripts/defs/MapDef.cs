using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;
using Godot.Serialization;
using Globals;
using Mapping;

namespace Defs
{
    public class MapDef : Def
    {
        public MapDef()
        {

        }

        private string mapBlueprintDef;
        private MapBlueprintDef _mapBlueprintDef;
        public MapBlueprintDef MapBlueprintDef
        {
            get
            {
                if (_mapBlueprintDef == null)
                {
                    _mapBlueprintDef = (MapBlueprintDef)DataParsing.GetNode(mapBlueprintDef);
                }
                return _mapBlueprintDef;
            }
        }

        


        

        public Map GenerateMap()
        {
            UI.MapScreen mapScreen = (UI.MapScreen)GetTree().Root.GetNode("Game/MapScreen");
            Map map = TranslateToMap();
            mapScreen.AddChild(map);

            return map;
        }

        private Map TranslateToMap()
        {
            PackedScene packedScene = (PackedScene)GD.Load("res://scenes/mapping/maps/_Base.tscn");
            Map map = (Map)packedScene.Instance();


            return map;
        }
    }
}


