using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;
using Globals;
using UI.Widgets;

namespace Defs
{
    public class OutputDef: Def
    {
        public OutputDef()
        {

        }

        private string nodeType;
        private Type _nodeType;
        public Type NodeType
        {
            get
            {
                if (_nodeType == null)
                {
                    try
                    {
                        _nodeType = System.AppDomain.CurrentDomain.GetAssemblies()
                            .SelectMany(x => x.GetTypes()
                            .Where(y => y.Namespace == typeof(LogOutput).Namespace)
                            ).First(x => x.Name == nodeType);

                    }
                    catch (Exception e)
                    {
                        GD.PrintErr($"Failed to get output node type with name: \"{nodeType}\" . Was the name entered correctly? Error: {e}");
                    }
                }
                return _nodeType;
            }
        }

        public string textContents;
    }
}


