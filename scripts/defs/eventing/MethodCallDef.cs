using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Godot;
using Godot.Serialization;
using Globals;

namespace Defs
{
    public class MethodCallDef: Def
    {
        public MethodCallDef()
        {

        }

        [Serialize]


        //either (the alias) or the (name and group) must be assigned in the def file
        private string targetName;
        private string targetGroup;
        private string targetAlias;
        private Node _target;
        public Node Target
        {
            get
            {
                if (_target == null)
                {
                    if (targetName != null && targetGroup != null)
                    {
                        try
                        {
                            IEnumerable<Node> nameMatches = GetTree().GetNodesInGroup(targetGroup).Cast<Node>().Where(node => node.Name == targetName);
                            if (nameMatches.Count() == 1)
                            {
                                _target = nameMatches.First();
                            }
                            else
                            {
                                GD.PrintErr($"Found a non-1 number of nodes with name: {targetName} in group: {targetGroup} for MethodCallDef {this.Name} .");
                                return null;
                            }
                        }
                        catch (Exception e)
                        {
                            GD.Print($"Failed to find targeted instance: {targetName} in targeted group: {targetGroup} for MethodCallDef {this.Name} . Error: {e}");
                        }
                    }
                    else if (targetAlias != null)
                    {
                        try
                        {
                            //switch statement for different aliases, like Player or Party etc
                        }
                        catch (Exception e)
                        {
                            GD.PrintErr($"Failed to find targeted instance with alias: {targetAlias} for MethodCallDef {this.Name} . Error: {e}");
                        }
                    }
                    else
                    {
                        GD.PrintErr($"Failed to get node for  for MethodCallDef {this.Name} . Insufficient details in XML.");
                    }
                }
                return _target;
            }
        }

        [Serialize]
        private string methodName;
        private MethodInfo _method;
        public MethodInfo Method
        {
            get
            {
                if (_method == null)
                {
                    _method = Target.GetType().GetMethod(methodName);
                }
                return _method; 
            }
        }


        private List<object> arguments;


        public void CallMethodFromDef()
        {
            if (arguments != null)
            {
                Target.GetType().InvokeMember(Method.Name, BindingFlags.InvokeMethod, null, Target, arguments.ToArray());
            }
            else
            {
                Target.GetType().InvokeMember(Method.Name, BindingFlags.InvokeMethod, null, Target, null);
            }
            
        }
    }
}


