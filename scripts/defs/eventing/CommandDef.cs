using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;
using Godot.Serialization;
using Globals;
using UI.Widgets;

namespace Defs
{
    public class CommandDef: Def
    {
        public CommandDef()
        {

        }

        public string eventToRun;
        private EventDef _eventToRun;
        public EventDef EventToRun
        {
            get
            {
                if (_eventToRun == null && eventToRun != null)
                {
                    _eventToRun = (EventDef)DataParsing.GetNode(eventToRun);
                }
                return _eventToRun;
            }
        }

        public string buttonLabel;

        [Serialize]
        public string buttonQueueLogic; //accepts FirstAvailable, LastAvailable, and ReserveIndex
        public int reservedIndex;

        //for command defs, tooltips are assumed to be the default
        public bool displayTooltip = true;
        public string tooltipName;
        public string tooltipDescription;
    }
}


