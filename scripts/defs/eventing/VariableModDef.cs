using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;
using Godot.Serialization;
using Globals;

namespace Defs
{
    public class VariableModDef: Def
    {
        public VariableModDef()
        {

        }

        [Serialize]
        private string target;
        public Node Target;

        [Serialize]
        public string variableName;

        [Serialize]
        public object value;
    }
}


