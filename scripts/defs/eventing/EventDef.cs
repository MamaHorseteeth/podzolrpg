using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;
using Globals;
using UI.Widgets;

namespace Defs
{
    public class EventDef: Def
    {
        public EventDef()
        {

        }

        private List<string> outputs;
        private List<OutputDef> _outputs;
        public List<OutputDef> Outputs
        {
            get
            {
                if (_outputs == null && outputs != null)
                {
                    _outputs = new List<OutputDef>();
                    for (int x = 0; x < outputs.Count; x++)
                    {
                        _outputs.Add((OutputDef)DataParsing.GetNode(outputs[x]));
                    }
                }
                return _outputs;
            }
        }

        private List<string> commands;
        private List<CommandDef> _commands;
        public List<CommandDef> Commands
        {
            get
            {
                if (_commands == null && commands != null)
                {
                    _commands = new List<CommandDef>();
                    for (int x = 0; x < commands.Count; x++)
                    {
                        _commands.Add((CommandDef)DataParsing.GetNode(commands[x]));
                    }
                }
                return _commands;
            }
        }

        private List<string> methodCalls;
        private List<MethodCallDef> _methodCalls;
        public List<MethodCallDef> MethodCalls
        {
            get
            {
                if (_methodCalls == null && methodCalls != null)
                {
                    _methodCalls = new List<MethodCallDef>();
                    for (int x = 0; x < methodCalls.Count; x++)
                    {
                        _methodCalls.Add((MethodCallDef)DataParsing.GetNode(methodCalls[x]));
                    }
                }
                return _methodCalls;
            }
        }

        private List<string> variableMods;
        private List<VariableModDef> _variableMods;
        public List<VariableModDef> VariableMods
        {
            get
            {
                if (_variableMods == null && variableMods != null)
                {
                    _variableMods = new List<VariableModDef>();
                    for (int x = 0; x < variableMods.Count; x++)
                    {
                        _variableMods.Add((VariableModDef)DataParsing.GetNode(variableMods[x]));
                    }
                }
                return _variableMods;
            }
        }
    }
}


