using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;
using Godot.Serialization;
using Globals;

namespace Defs
{
    public class SubspeciesDef : Def
    {
        

        //---Data---

        public SubspeciesDef()
        {

        }

        [Serialize]
        private string species;
        private SpeciesDef _species;
        public SpeciesDef Species { 
            get
            {
                if (_species == null) {
                    _species = (SpeciesDef)DataParsing.GetNode(species);
                }
                return _species;
            }
        }
    }
}


