using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;
using Godot.Serialization;
using Globals;

namespace Defs
{
    public class Def : Node
    {
        public Def()
        {

        }

        //---Shortcuts---
        private DataParsing _dataParsing;
        public DataParsing DataParsing
        {
            get
            {
                if (_dataParsing == null)
                {
                    _dataParsing = (DataParsing)GetTree().Root.GetNode("DataParsing");
                }
                return _dataParsing;
            }
        }

        [Serialize]
        public string defName;

        //while this seems like a place to use a dictionary and enums, i'm not really quite sure how to put it together
        public string SingleNeutral { get; set; }
        private string _singleFeminine;
        public string SingleFeminine
        {
            get
            {
                if (_singleFeminine == null)
                {
                    _singleFeminine = SingleNeutral;
                }
                return _singleFeminine;
            }
            set{_singleFeminine = value;}
        }
        private string _singleMasculine;
        public string SingleMasculine
        { 
            get
            {
                if (_singleMasculine == null)
                {
                    _singleMasculine = SingleNeutral;
                }
                return _singleMasculine;
            }
            set { _singleFeminine = value; }
        }
        private string _pluralNeutral;
        public string PluralNeutral
        {
            get
            {
                if (_pluralNeutral == null)
                {
                    string plural = SingleNeutral;
                    if (plural.EndsWith("s"))
                    {
                        plural += "e";
                    }
                    plural += "s";
                    _pluralNeutral = plural;
                }
                return _pluralNeutral;
            }
            set { _pluralNeutral = value; }
        }
        private string _pluralFeminine;
        public string PluralFeminine
        {
            get
            {
                if (_pluralFeminine == null)
                {
                    string plural = SingleFeminine;
                    if (plural.EndsWith("s"))
                    {
                        plural += "e";
                    }
                    plural += "s";
                    _pluralFeminine = plural;
                }
                return _pluralFeminine;
            }
            set { _pluralFeminine = value; }
        }
        private string _pluralMasculine;
        public string PluralMasculine
        {
            get
            {
                if (_pluralMasculine == null)
                {
                    string plural = SingleMasculine;
                    if (plural.EndsWith("s"))
                    {
                        plural += "e";
                    }
                    plural += "s";
                    _pluralMasculine = plural;
                }
                return _pluralMasculine;
            }
            set { _pluralMasculine = value; }
        }
    }
}