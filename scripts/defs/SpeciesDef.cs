using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;
using Globals;

namespace Defs
{
    public class SpeciesDef : Def
    {
        public SpeciesDef()
        {

        }

        public List<SubspeciesDef> Subspecies { get; set; }
    }
}


