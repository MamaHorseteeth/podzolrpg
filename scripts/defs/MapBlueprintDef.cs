using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;
using Godot.Serialization;
using Globals;
using Mapping;

namespace Defs
{
    public class MapBlueprintDef : Def
    {
        public MapBlueprintDef()
        {

        }

        //again, probably a situation where we'd want to use enums but i do not understand them very well. acceptable values:
        // - StaticLayout : use a pre-drawn map, defined by staticMapLayout
        [Serialize]
        public string mapGenProtocol;

        private Dictionary<int, string> tileOptions = new Dictionary<int, string>();
        private Dictionary<int, TileDef> _tileOptions = new Dictionary<int, TileDef>();
        public Dictionary<int, TileDef> TileOptions
        {
            get
            {
                if (!_tileOptions.Any())
                {
                    //for each tileOption, translate into a _tileOption and add it to there
                    for(int x = 0; x < tileOptions.Count; x++)
                    {
                        TileDef tileDef = (TileDef)DataParsing.GetNode(tileOptions[x]);
                        _tileOptions.Add(x, tileDef);
                    }
                }
                return _tileOptions;
            }
        }

        //used only for maps with StaticLayout as their mapGenProtocol
        private Dictionary<int, List<string>> staticMapLayout = new Dictionary<int, List<string>>();
        private Dictionary<int, List<TileDef>> _staticMapLayout = new Dictionary<int, List<TileDef>>();
        public Dictionary<int, List<TileDef>> StaticMapLayout
        {
            get
            {
                if (!_staticMapLayout.Any())
                {
                    //for each row
                    for (int x = 0; x < staticMapLayout.Count; x++)
                    {
                        //for each column in that row
                        for (int y = 0; y < staticMapLayout[x].Count; y++)
                        {

                        }
                        
                        
                        
                    }
                }
                return _staticMapLayout;
            }
        }

        //only required by randomly-generated maps
        public int minX;
        public int minY;
        public int maxX;
        public int maxY;
        public Vector2 RandomDimensions
        {
            get
            {
                Random r = new Random();

                int x = r.Next(minX, maxX);
                int y = r.Next(minY, maxY);

                return new Vector2(x, y);
            }
        }



    }
}


