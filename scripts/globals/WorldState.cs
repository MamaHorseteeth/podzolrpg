using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace Globals
{
    /// <summary>
    /// active parties are parties of characters, both loaded and unloaded, whose status as a coherent group is retained when unloaded
    /// </summary>
    public class WorldState : Node
    {
        private List<People.Party> _activeParties = new List<People.Party>();
        public List<People.Party> ActiveParties
        {
            get
            {
                return _activeParties;
            }
        }


        /// <summary>
        /// loose characters are characters who are not currently loaded and whose party affiliations are not retained when unloaded
        /// an example would be roving wilders, who don't habitually hang with the same bands for very long unless they manage to develop some kind of relationship with each other. on a re-encounter, they might be somewhere on the other side of the world, with completely different people
        /// </summary>
        private List<People.Character> _looseCharacters;
        public List<People.Character> LooseCharacters
        {
            get
            {
                return _looseCharacters;
            }
        }


        public override void _Ready()
        {

        }
    }
}