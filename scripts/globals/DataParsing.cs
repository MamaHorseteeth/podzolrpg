using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

using Godot;
using Godot.Serialization;
using Defs;

namespace Globals
{
    public class DataParsing : Node
    {
        private List<string> dataFilePaths = new List<string> { };
        private Serializer serializer = new Serializer();

        public List<Node> defDatabase = new List<Node> { };

        //---Globals---
        private Events.EventEngine _eventEngine;
        public Events.EventEngine EventEngine
        {
            get
            {
                if (_eventEngine == null)
                {
                    _eventEngine = (Events.EventEngine)GetTree().Root.GetNode("Game/EventEngine");
                }
                return _eventEngine;
            }
        }

        public override void _Ready()
        {
            //put def files in this directory or they won't be used
            //we'll probably have to do more here for mod support, but i don't want to think about mod support until there's something to mod in the first place
            dataFilePaths = GetFilesWithExtension(".xml", "res://defs/");

            for (int x = 0; x < dataFilePaths.Count; x++)
            {
                ReadXmlFile(dataFilePaths[x]);
            }

            //now that defs are loaded, it's time to start the game, which requires launching an initialization event
            //launch the initialization event that throws up our menu keys
            EventEngine.RunEvent(new Events.Utility.Initialization());
        }

        //converted to c# from the code provided at https://godotengine.org/qa/5175/how-to-get-all-the-files-inside-a-folder by user anachronost
        //modified to specify files with a given extension
        public List<string> GetFilesWithExtension(string extension, string targetDirectory = "res://")
        {
            List<string> files = new List<string> { };
            List<string> directories = new List<string> { };
            Directory directory = new Directory();

            if (directory.Open(targetDirectory) == Error.Ok)
            {
                directory.ListDirBegin(true, false);
                GatherFiles(directory, files, directories, extension);
            }
            else
            {
                GD.PrintErr("Failed to open XML directory. Has the file path changed?");
            }

            return files;
        }

        private void GatherFiles(Directory directory, List<string> files, List<string> directories, string extension)
        {
            string fileName = directory.GetNext();

            while (fileName != "")
            {
                string path = directory.GetCurrentDir() + "/" + fileName;
                if (directory.CurrentIsDir())
                {
                    //GD.Print("Found directory: " + path);
                    Directory subDirectory = new Directory();
                    subDirectory.Open(path);
                    subDirectory.ListDirBegin(true, false);
                    directories.Add(path);
                    GatherFiles(subDirectory, files, directories, extension);
                }
                else if(path.EndsWith(extension))
                {
                    //GD.Print("Found xml file: " + path);
                    files.Add(path);
                }
                fileName = directory.GetNext();
            }
            directory.ListDirEnd();
        }

        private void ReadXmlFile(string filePath)
        {
            //opens the file
            File file = new File();
            file.Open(filePath, File.ModeFlags.Read);
            string fileText = file.GetAsText();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(fileText);

            XmlElement root = doc.DocumentElement;

            //only do anything if the document's root node is <Defs>. if we must have a root, this allows us to use that as as a signifier that the contents of this file are defs and thus need parsed
            if (root.Name == "Defs")
            {
                //for each def in the file, deserialize into a def. ignores comments
                IEnumerable<XmlNode> defNodes = root.ChildNodes.Cast<XmlNode>().Where(node => node.NodeType != XmlNodeType.Comment);
                foreach(XmlNode defData in defNodes)
                {
                    try
                    {
                        Def def = (Def)serializer.Deserialize(defData);
                        def.Name = def.defName;
                        defDatabase.Add(def);
                        this.AddChild(def);
                    }
                    catch (Exception e)
                    {
                        GD.PrintErr(e);
                    }
                }
            }
        }





        //private Dictionary<string, object> ReadJsonFile(string filePath)
        //{
        //    File file = new File();
        //    string fileText;

        //    //opens the file in readonly mode
        //    file.Open(filePath, File.ModeFlags.Read);
        //    //gets a string of the file's entire contents, converts it into a json parse result, and if it parsed as a godot dictionary,
        //    fileText = file.GetAsText();
        //    var parseResult = JSON.Parse(fileText);
        //    if (parseResult.Error == Error.Ok)
        //    {
        //        if (parseResult.Result.GetType() == typeof(Godot.Collections.Dictionary))
        //        {
        //            //GD.Print("JSON file successfully parsed!");

        //            //converts the godot dictionary to a c# dictionary
        //            Godot.Collections.Dictionary godotDict = (Godot.Collections.Dictionary)parseResult.Result;
        //            return CastGodotDictToDictionary(godotDict);
        //        }
        //        else if (parseResult.Result.GetType() == typeof(Godot.Collections.Array))
        //        {
        //            GD.PrintErr("JSON file parsed as an array, which is not the correct format.");
        //            return null;
        //        }
        //        else
        //        {
        //            GD.PrintErr("JSON file somehow didn't parse as a dictionary or array, which probably means I got the dictionary type wrong.");
        //            return null;
        //        }
        //    }
        //    else
        //    {
        //        GD.PrintErr("Failed to parse JSON file: " + filePath + " , at line: " + parseResult.ErrorLine + " with error message: " + parseResult.ErrorString);
        //        return null;
        //    }
        //}

        //private Dictionary<string, object> CastGodotDictToDictionary(Godot.Collections.Dictionary godotDict)
        //{
        //    try
        //    {
        //        Dictionary<string, object> convertedDictionary = godotDict.Keys.Cast<string>().ToDictionary<string, string, object>(key => key, key => (object)godotDict[key]);
        //        return convertedDictionary;
        //    }
        //    catch(Exception e)
        //    {
        //        GD.PrintErr("Failed to cast Godot dictionary: " + godotDict + " to C# dictionary. Error: " + e);
        //        return null;
        //    }
        //}

        //private List<object> CastGodotArrayToList(Godot.Collections.Array godotArray)
        //{
        //    List<object> convertedArray = godotArray.Cast<object>().ToList();
        //    return convertedArray;
        //}

        //private void CreateDefsFromJsonDictionary(Dictionary<string, object> dictionary)
        //{
        //    //so the anatomy of our json files is thus:
        //    //a quirk of json is that there can only be one element at the top of the dictionary
        //    //our topmost element is a dictionary, which contains one key-value pair: a string and a dictionary
        //    //- the key in this top pair is what we use to specify the class we're trying to instance
        //    //- the value is a string-dict dictionary of defs and their properties
        //    //- - the keys in this dictionary are the unique ids of each def
        //    //- - the values are string-object dictionaries of properties and their values
        //    //- - - the keys in this dictionary correspond directly to the name of a field or property
        //    //- - - the values in this dictionary are to be assigned as the values of those fields or properties

        //    //finds a class with the same name as the key
        //    string headKey = dictionary.ElementAt(0).Key;
        //    string currentKey = headKey;
        //    Type defType = null;
        //    try
        //    {
        //        defType = System.AppDomain.CurrentDomain.GetAssemblies()
        //            .SelectMany(x => x.GetTypes()
        //            .Where(y => y.Namespace == nameof(Defs)))
        //            .First(x => x.Name == headKey);
        //    }
        //    catch (Exception e)
        //    {
        //        GD.PrintErr("Failed to parse dictionary with top-level key: " + dictionary.ElementAt(0).Key + " . Ensure that the JSON file's topmost key corresponds to an exisiting Def class. Error: " + e);
        //    }

        //    //check to make sure that the file is written correctly with a dictionary as the value of the head of the tree
        //    if (dictionary[headKey].GetType() == typeof(Godot.Collections.Dictionary))
        //    {
        //        //convert the value dictionary (defs-properties) from godot to c#
        //        Dictionary<string, object> defDict = CastGodotDictToDictionary((Godot.Collections.Dictionary)dictionary[headKey]);
        //        //for each def in the dict (that is, every def in the file), call the create def method
        //        for (int x = 0; x < defDict.Count; x++)
        //        {
        //            CreateDef(defType, defDict.ElementAt(x));
        //        }
        //    }
        //    else
        //    {
        //        GD.PrintErr("Failed to parse dictionary with top-level key: " + dictionary.ElementAt(0).Key + " because top-level value is not a dictionary.");
        //    }
        //}

        //private void CreateDef(Type defType, KeyValuePair<string, object> defData)
        //{
        //    //create an instance of the basic def and add it as a child of this autoload, so that (if i understand correctly) it gets freed on game closure
        //    //also adds it to a central list of defs that exist
        //    Node def = (Node)Activator.CreateInstance(defType);
        //    AddChild(def);
        //    defDatabase.Add(def);

        //    //set the node's name to the key
        //    def.Name = defData.Key;

        //    //now cast the data's value type from obj to a string-obj dict
        //    Dictionary<string, object> defProperties = CastGodotDictToDictionary((Godot.Collections.Dictionary)defData.Value);

        //    //for each property that is a key in the dictionary, try and assign the property
        //    for (int x = 0; x < defProperties.Count; x++)
        //    {
        //        string propertyName = defProperties.ElementAt(x).Key;
        //        object propertyValue = defProperties.ElementAt(x).Value;

        //        try
        //        {
        //            PropertyInfo propertyInfo = def.GetType().GetProperty(propertyName);
        //            Type dataType = propertyValue.GetType();
        //            Type propertyType = propertyInfo.PropertyType;

        //            //if the type does not match, we need to cast the json type as the proper type
        //            //my understanding of my json anatomy is that pretty much the only time this should happen are when one def has properties whose types are:
        //            //other defs
        //            //godot arrays of other defs
        //            if (dataType != propertyType)
        //            {
        //                GD.Print(propertyName + " is: " + dataType + " in JSON but is: " + propertyType + " in def script.");

        //                if (dataType == typeof(string))
        //                {
        //                    string valueString = (string)propertyValue;
        //                    //deferred, to ensure that all defs are generated before we go looking for nodes whose ids match the string
        //                    CallDeferred(nameof(CastJsonStringToDef), propertyValue, valueString, def, propertyInfo);
        //                }
        //                else if (dataType == typeof(Godot.Collections.Array))
        //                {
        //                    //could be upgraded to work recursively but i don't really have any contexts in mind where that would be useful
        //                    List<object> defListData = CastGodotArrayToList((Godot.Collections.Array)propertyValue);
        //                    for (int y = 0; y < defListData.Count; y++)
        //                    {
        //                        string valueString = (string)defListData[y];
        //                        //deferred, to ensure that all defs are generated before we go looking for nodes whose ids match the string
        //                        CallDeferred(nameof(CastJsonStringToDef), propertyValue, valueString, def, propertyInfo);
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                propertyInfo.SetValue(def, propertyValue);
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            //GD.PrintErr("Failed to assign value of key named: " + propertyName + " to def: " + defData.Key + " . Possible causes: typo in JSON file or Def script? Def variable set as a field instead of a property? Error: " + e);
        //        }
        //    }
        //}

        //private void CastJsonStringToDef(ref object propertyValue, string valueString, Node def, PropertyInfo propertyInfo)
        //{
        //    propertyValue = this.GetNode(valueString);
        //    propertyInfo.SetValue(def, propertyValue);
        //}

        //private void ParseXml()
        //{
        //    string fileName;

        //    for(int x = 0; x < xmlFilePaths.Count; x++)
        //    {
        //        fileName = xmlFilePaths[x];
        //        try
        //        {
        //            parser.Open(fileName);
        //        } 
        //        catch (Exception e)
        //        {
        //            GD.PrintErr("Failed to open XML file: " + fileName + " ! Error: " + e);
        //        }

        //        //reads until we run out of defs to process
        //        while (parser.Read() == Error.Ok)
        //        {
        //            string nodeName = parser.GetNodeName();
        //            bool withinDefs = false;
        //            bool within

        //            object value;



        //            //the most reasonable approach to me seems like a switch statement for each relevant type of xml node
        //            switch (parser.GetNodeType())
        //            {
        //                case XMLParser.NodeType.Element:
        //                    if (nodeName == "Defs")
        //                    {
        //                        withinDefs = true;
        //                    }
        //                    if (withinDefs == true)
        //                    {

        //                    }
        //                    break;
        //                case XMLParser.NodeType.Text:
        //                    break;
        //                case XMLParser.NodeType.ElementEnd:
        //                    if (nodeName == "Defs")
        //                    {
        //                        withinDefs = false;
        //                    }
        //                    break;
        //                default:
        //                    break;
        //            }
        //        }
        //    }
        //}

    }
}


