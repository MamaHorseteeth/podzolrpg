using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace Globals
{
    public class Mapping : Node
    {
        //keys are directions; values are bools indicating if a direction represents an increase in the vector2.x and vector2.y respectively. null means no change
        //apparently you can invoke, like, Vector2.Left and so on, rendering this system kind of redundant?
        public Dictionary<string, bool?[]> _validDirections = new Dictionary<string, bool?[]>
    {
        { "north", new bool?[]{null, false} },
        { "south", new bool?[]{null, true} },
        { "west", new bool?[]{false, null} },
        { "east", new bool?[]{true, null} },
        { "northwest", new bool?[]{false, false} },
        { "northeast", new bool?[]{true, false} },
        { "southwest", new bool?[]{false, true} },
        { "southeast", new bool?[]{true, true} },
    };

        
        public Dictionary<string[], object> directionAliases = new Dictionary<string[], object>
    {
        {new string[] { "n", "N", "north", "North", "NORTH", "up", "Up", "UP" }, "north" },
        {new string[] { "s", "S", "south", "South", "SOUTH", "down", "Down", "DOWN" }, "south" },
        {new string[] { "w", "W", "west", "West", "WEST", "left", "Left", "LEFT" }, "west" },
        {new string[] { "e", "E", "east", "East", "EAST", "right", "Right", "RIGHT" }, "east" },
        {new string[] { "nw", "NW", "northwest", "Northwest", "NORTHWEST" }, "northwest" },
        {new string[] { "ne", "NE", "northeast", "Northeast", "NORTHEAST" }, "northeast" },
        {new string[] { "sw", "SW", "southwest", "Southwest", "SOUTHWEST" }, "southwest" },
        {new string[] { "se", "SE", "southeast", "Southeast", "SOUTHEAST" }, "southeast" },
    };

        //---Shortcuts---
        private UI.MapScreen _mapScreen;
        public UI.MapScreen MapScreen
        {
            get
            {
                if (_mapScreen == null)
                {
                    _mapScreen = GetTree().Root.GetNode<UI.MapScreen>("MapScreen");
                }
                return _mapScreen;
            }
        }

        public override void _Ready()
        {
        }
    }
}