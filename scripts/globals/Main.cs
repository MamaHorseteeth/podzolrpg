using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace Globals
{
    public class Main : Node
    {
        //---Menu Switching---
        public UI.Menus.Menu CurrentMenu { get; set; }
        public List<string> previousMenuPaths = new List<string> { };//takes file paths of previous menus

        //the scene on which nodes are built
        private Node _backdrop;
        public Node Backdrop
        {
            get
            {
                if (_backdrop == null)
                {
                    _backdrop = GetTree().Root.GetNode("Backdrop");
                }
                return _backdrop;
            }
        }

        private Events.EventEngine _eventEngine;
        public Events.EventEngine EventEngine
        {
            get
            {
                if (_eventEngine == null)
                {
                    _eventEngine = (Events.EventEngine)GetTree().Root.GetNode("Game/EventEngine");
                }
                return _eventEngine;
            }
        }

        //---User Settings---
        public string saveFileName;

        //---READY--

        public override void _Ready()
        {
            this.AddToGroup("Globals");
            
            //in theory we could also have a randomizing function here perhaps

            //original location of gamestart event
        }

        //---USER SETTINGS---

        //keyboard/keybinding compatibility can come later

        //--- MENU SWITCHING ---
        //modified from existing godot tutorial       

        public void SwitchMenu(string sceneResourcePath, bool runEventOnSwitch = false, string eventDefName = null)
        {
            //waits for any running code to stop before switching the scene
            //i believe, but i'm not quite sure, that code set to loop perpetually will have to be stopped in order for this to work?
            CallDeferred(nameof(DeferredSwitchMenu), sceneResourcePath, runEventOnSwitch, eventDefName);
        }

        private void DeferredSwitchMenu(string sceneResourcePath, bool runEventOnSwitch, string eventDefName)
        {
            //technically, we could probably write this in such a way that it caches previous menus rather than reloads them wholecloth, but menus don't take up a whole lot of processing power to create
            
            //removes current scene, if one has been assigned
            if (CurrentMenu != null)
            {
                CurrentMenu.Free();
            }

            //loads and instances a new scene, then adds it as a child of our backdrop
            CurrentMenu = (UI.Menus.Menu)LoadSceneFromFile(sceneResourcePath);
            Backdrop.AddChild(CurrentMenu);

            //if there's an event queued, run it now
            if (runEventOnSwitch == true)
            {
                EventEngine.RunEvent(eventDefName);
            }
        }

        //---MISC---

        //loads and instances a new scene from its filepath with a built-in error message that helps find bad and outdated filepaths
        public Node LoadSceneFromFile(string sceneResourcePath)
        {
            try
            {
                PackedScene packedScene = (PackedScene)GD.Load(sceneResourcePath);
                Node instancedScene = packedScene.Instance();
                return instancedScene;
            }
            catch (Exception e)
            {
                GD.PrintErr(nameof(LoadSceneFromFile) + " failed. Was the target path entered correctly? Was the target scene relocated in the files? Error message: " + e);
                return null;
            }
        }

        //for finding an alias in a dictionary of aliases 
        public object TryFindAlias(Dictionary<string[], object> dictionary, string alias)
        {
            for (int x = 0; x < dictionary.Count; x++)
            {
                string[] aliasSet = dictionary.ElementAt(x).Key;
                if (aliasSet.Contains(alias))
                {
                    //return the value associated with the alias
                    return dictionary[aliasSet];
                }
            }
            //if we haven't returned anything yet, then we need to return null
            return null;
        }

        public void QuitToDesktop()
        {
            GetTree().Quit();
        }
    }
}