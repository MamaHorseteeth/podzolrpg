﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;
using UI.Menus;
using UI.Widgets;

namespace Events.Utility
{
    public class GenerateWorld : Event
    {
        public override void Run(LogSubmenu logSubmenu, CommandPanel commandPanel)
        {
            //deeper world generation logic goes here





            //we also need to generate the default void map, which contains the initial player-controlled party and a blank player character
            //generate the 1x1 "undefined" map and its tiles
            UI.MapScreen mapScreen = (UI.MapScreen)EventEngine.GetTree().Root.GetNode("Game/MapScreen");
            PackedScene packedMap = (PackedScene)GD.Load("res://scenes/mapping/maps/CharGenVoid.tscn");
            Mapping.Map map = (Mapping.Map)packedMap.Instance();
            mapScreen.AddChild(map);

            //assign the party in that scene to the worldstate list of active parties
            People.Party playerParty = (People.Party)map.GetNode("Tile/PlayerParty");
            WorldState.ActiveParties.Add(playerParty);

            //tell the party viewer to display data for our current party & character
            //the setter runs PartyViewer.UpdateDisplay() every time CurrentParty gets set, to ensure that the information in the viewer is never out of date
            PartyViewer partyViewer = (PartyViewer)EventEngine.GetTree().Root.GetNode("Game/PlayMenu/Control/LeftColumn/PartyViewer");
            partyViewer.CurrentParty = playerParty;
            //then unhide the (player) party view widget on the lefthand sidebar
            partyViewer.Show();

            Event nextEvent = GetExistingOrNewEvent(typeof(CharGen.CustomizationOptions));
            EventEngine.RunEvent(nextEvent);
        }
    }
    
    public class Initialization : Event
    {
        public override void Run(LogSubmenu logSubmenu, CommandPanel commandPanel)
        {
            TextOutput message = (TextOutput)LoadOutputFromFile(typeof(TextOutput));
            message.Label.BbcodeText = $"Welcome to Podzol RPG! This main menu is still under construction!\nHere's a linebreak to test if the deserializer can handle paragraphs right out the box.";
            logOutputs.Add(message);

            Event newGameEvent = GetExistingOrNewEvent(typeof(NewGame));
            Command newGameCommand = new Command(newGameEvent, "New Game", "ReserveIndex", 0);
            newGameCommand.tooltipDescription = $"Start a new life, in the teeming mycology of Podzol!";
            commands.Add(newGameCommand);

            base.Run(logSubmenu, commandPanel);
        }
    }

    public class NewGame : Event
    {
        public override void Run(LogSubmenu logSubmenu, CommandPanel commandPanel)
        {
            TextOutput message = (TextOutput)LoadOutputFromFile(typeof(TextOutput));
            message.Label.BbcodeText = $"Podzol RPG is an erotic text adventure, similar to famous titles like Corruption of Champions, Trials in Tainted Space, and Lilith's Throne. As such, if you are not of legal age to view pornographic content in your region, you ought to quit and uninstall the game immediately.\nFurthermore, Podzol RPG explores fetishes that fans of these aforementioned games might not be interested in; particularly vore (endo and fatal, primarily soft, with potential but not guaranteed reformation). Podzol RPG also explores physical injury and death in more detail than usual for the genre. The only thing I can confirm will not be appearing in this game is anything to do with minors. If any of this content is displeasurable to you, any displeasure that results from playing regardless is your own responsibility, not mine.\n\nAll characters depicted are of the physical, mental, and social age of 18, if not older. All characters are born or otherwise created at this age.\nDepiction does not equal endorsement.\n\nDo you understand and accept this disclaimer?";
            logOutputs.Add(message);

            Event acceptDisclaimerEvent = GetExistingOrNewEvent(typeof(ReviewWorldSettings));
            Command acceptDisclaimerCommand = new Command(acceptDisclaimerEvent, "Yes", "ReserveIndex", 0);
            acceptDisclaimerCommand.tooltipDescription = $"Accept my terms and conditions, and continue to world settings.";
            commands.Add(acceptDisclaimerCommand);

            Event rejectDisclaimerEvent = GetExistingOrNewEvent(typeof(QuitGame));
            Command rejectDisclaimerCommand = new Command(rejectDisclaimerEvent, "No", "ReserveIndex", 1);
            rejectDisclaimerCommand.tooltipDescription = $"Reject my terms and conditions, and immediately close the game.";
            commands.Add(rejectDisclaimerCommand);

            base.Run(logSubmenu, commandPanel);
        }
    }

    public class QuitGame : Event
    {
        public override void Run(LogSubmenu logSubmenu, CommandPanel commandPanel)
        {
            EventEngine.GetTree().Quit();
        }
    }

    public class ReviewWorldSettings : Event
    {
        public override void Run(LogSubmenu logSubmenu, CommandPanel commandPanel)
        {
            TextOutput message = (TextOutput)LoadOutputFromFile(typeof(TextOutput));
            message.Label.BbcodeText = $"Alrighty, then! We can continue!\n\nFirst, however, please take a moment to adjust your world settings (copied from your universal settings) to whatever parameters you would like for this particular world. When you are done, you may move on using the Continue command.";
            logOutputs.Add(message);

            //commands for opening the options menu go here

            Event continueEvent = GetExistingOrNewEvent(typeof(GenerateWorld));
            Command continueCommand = new Command(continueEvent, "Continue", "LastAvailable");
            continueCommand.tooltipDescription = $"Confirm initial world settings and move on to character creation!";
            commands.Add(continueCommand);

            base.Run(logSubmenu, commandPanel);
        }
    }
}
