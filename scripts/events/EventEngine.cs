using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;
using UI.Menus;
using UI.Widgets;

namespace Events
{
    //What is an event?
    //an event is the series of actions which pressing a button (command keys, movement keys, new game keys, etc) yields,
    //which includes things like moving the player's party to a new tile, adding new outputs to the log, and calculating the results of item use or a combat turn
    //this autoload is the centralized engine through which events are input
    public class EventEngine : Node
    {
        public List<Event> events = new List<Event>();

        //---Shortcuts---

        private LogSubmenu _logSubmenu;
        public LogSubmenu LogSubmenu
        {
            get
            {
                if (_logSubmenu == null)
                {
                    try
                    {
                        _logSubmenu = (LogSubmenu)GetTree().Root.GetNode("Game/PlayMenu/Control/SubmenuScreen/LogSubmenu");
                    }
                    catch (Exception e)
                    {
                        GD.PrintErr($"Failed to find {nameof(LogSubmenu)}. Error: {e}");
                    }
                }
                return _logSubmenu;
            }
        }

        private CommandPanel _commandPanel;
        public CommandPanel CommandPanel
        {
            get
            {
                if (_commandPanel == null)
                {
                    try
                    {
                        _commandPanel = (CommandPanel)GetTree().Root.GetNode("Game/PlayMenu/Control/CommandPanel");
                    }
                    catch (Exception e)
                    {
                        GD.PrintErr($"Failed to find {nameof(CommandPanel)}. Error: {e}");
                    }
                }
                return _commandPanel;
            }
        }

        //---Globals---

        private Globals.Main _globalMain;
        public Globals.Main GlobalMain
        {
            get
            {
                if (_globalMain == null)
                {
                    _globalMain = (Globals.Main)GetTree().Root.GetNode("Game/EventEngine");
                }
                return _globalMain;
            }
        }

        private Globals.DataParsing _data;
        public Globals.DataParsing Data
        {
            get
            {
                if (_data == null)
                {
                    _data = (Globals.DataParsing)GetTree().Root.GetNode("DataParsing");
                }
                return _data;
            }
        }

        //this version is more or less for when we have to launch a specific event that we know by name and know won't change, such as the New Game event
        public void RunEvent(string eventDefName)
        {
            //try
            //{
            //    EventDef eventDef = (EventDef)Data.GetNode(eventDefName);
            //    RunEvent(eventDef);
            //}
            //catch (Exception e)
            //{
            //    GD.PrintErr($"Failed to get an EventDef with the name: {eventDefName} ! Is the name correct? Error: {e}");
            //}
            
        }

        public void RunEvent(Event eventToRun)
        {
            eventToRun.EventEngine = this;
            eventToRun.Run(LogSubmenu, CommandPanel);
        }
    }

    public class Event
    {
        public EventEngine EventEngine { get; set; }
        public List<LogOutput> logOutputs = new List<LogOutput>();
        public List<Command> commands = new List<Command>();

        private Globals.WorldState _worldState;
        public Globals.WorldState WorldState
        {
            get
            {
                if (_worldState == null)
                {
                    _worldState = (Globals.WorldState)EventEngine.GetTree().Root.GetNode("WorldState");
                }
                return _worldState;
            }
        }



        //an event knows its own settings, calls, outputs, and commands, which it then acts on in sequence
        //default functionality will be to only process outputs and commands, allowing us to use base.Run() if inheritors do not need to intermingle settings/calls with outputs/commands
        public virtual void Run(LogSubmenu logSubmenu, CommandPanel commandPanel)
        {
            AddOutputsToLog(logOutputs, logSubmenu);
            AssignCommands(commands, commandPanel);
        }

        public virtual void AddOutputsToLog(List<LogOutput> logOutputs, LogSubmenu logSubmenu)
        {
            //we'll probably want to implement log pagination logic in here as well, later
            //it'll probably be done by incrementing a number of events and, when it reaches a user-set limit, starting a new page



            for (int x = 0; x < logOutputs.Count; x++)
            {
                logSubmenu.LogEntries.AddChild(logOutputs[x]);

                //just add a newline after every outut to ensure proper separation
                //TextOutput spacer = (TextOutput)LoadOutputFromFile(typeof(TextOutput));
                //spacer.Label.BbcodeText = $"\n";
                //logSubmenu.LogEntries.AddChild(spacer);
            }
        }

        //this exists separate because even if we do not call base.Run in the instanced event, we might still want to use standard command assignment
        public virtual void AssignCommands(List<Command> commands, CommandPanel commandPanel)
        {
            commandPanel.AssignEventCommandsToButtons(commands);
        }

        protected Node LoadOutputFromFile(Type outputType)
        {
            try
            {
                //packed scene assignment block. potentially able to be moved to switch-case format?
                PackedScene packedScene = null;
                if (outputType == typeof(TextOutput))
                {
                    packedScene = (PackedScene)GD.Load("res://scenes/ui/widgets/outputs/TextOutput.tscn");
                }
                else if(outputType == typeof(LineInput))
                {
                    packedScene = (PackedScene)GD.Load("res://scenes/ui/widgets/outputs/LineInput.tscn");
                }
                
                //if we got something, return it; else, throw error and return null
                if (packedScene != null)
                {
                    Node instancedScene = packedScene.Instance();
                    return instancedScene;
                }
                else
                {
                    
                    return null;
                }
            }
            catch (Exception e)
            {
                GD.PrintErr($"Failed to load log output scene from file of type: {outputType} . Error: {e}" );
                return null;
            }
        }

        protected Event GetExistingOrNewEvent(Type type, string context = null)
        {
            IEnumerable<Event> existingEvents = EventEngine.events?.Where(ev => ev.GetType() == type);
            //code for context selection
            //just go with first for now
            if (existingEvents != null && existingEvents.Any() == true)
            {
                Event chosenEvent = existingEvents?.First();
                return chosenEvent;
            }
            else
            {
                Event newEvent = (Event)Activator.CreateInstance(type);
                EventEngine.events.Add(newEvent);
                return newEvent;
            }
        }
    }

    public class Command
    {
        public Event eventToRun;

        //should prolly be an enum but i get headaches working with those. valid strings:
        // ReserveIndex : Uses reservedIndex to place command in a specific slot on the command panel
        // FirstAvailable : Places command in the first empty slot on the command panel
        // LastAvailable : Places command on last available slot in the current cycle of the command panel.
        public string buttonQueueLogic;
        public int reservedIndex; //doesn't do anything if QueueLogic is not ReserveIndex. multiple commands on the same event should never reserve the same index or shit will break

        public string buttonLabelText;

        public bool displayTooltip = true; //for command buttons, the presence of tooltips is assumed to be the default
        public string tooltipName; //and the tooltip name is usually the command name
        public string tooltipDescription;

        public Command(Event eventToRun, string buttonLabelText, string buttonQueueLogic, int queueIndexIfReserved = 0, bool distinctTooltipName = false)
        {
            this.eventToRun = eventToRun;
            this.buttonQueueLogic = buttonQueueLogic;
            this.reservedIndex = queueIndexIfReserved;

            this.buttonLabelText = buttonLabelText;
            if (distinctTooltipName == false)
            {
                this.tooltipName = buttonLabelText;
            }
        }
    }
}