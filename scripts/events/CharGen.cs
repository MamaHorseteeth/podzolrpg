﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;
using UI.Menus;
using UI.Widgets;

namespace Events.CharGen
{
    public class CustomizationOptions : Event
    {
        public override void Run(LogSubmenu logSubmenu, CommandPanel commandPanel)
        {
            


            TextOutput message = (TextOutput)LoadOutputFromFile(typeof(TextOutput));
            message.Label.BbcodeText = $"So it shall be.\nNow, as far as your starting character is concerned, I'm sure were can find some poor sap to your exacting preferences, somewhere in the world. Go ahead; play with the options.";
            logOutputs.Add(message);

            base.Run(logSubmenu, commandPanel);
        }
    }
}
